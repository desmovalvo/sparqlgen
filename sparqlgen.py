#!/usr/bin/python3

# global reqs
import os
import json
import uuid
import logging
import websocket
import subprocess
import configparser
from os import environ
from flask import Flask, request
from websocket import create_connection

# create a Flask app     
app = Flask(__name__)

# flask routes
@app.route('/sparqlgen', methods=["POST"])
def translate():

    # debug print
    logging.debug("New SPARQLgenerate request received!")
    
    # read the input
    query = request.form["query"]

    # generate names for temporary input and output files
    tmpID = str(uuid.uuid4())
    tmpIn = tmpID + "_IN"
    tmpOut = tmpID + "_OUT"

    # copy the query to a temporary local file
    tmpInFile = open(tmpIn, "w")
    tmpInFile.write(query)
    tmpInFile.close()
    
    # call a subprocess
    logging.debug("Invoking sparql-generate-jena")
    subprocess.run(["java", "-jar", "sparql-generate-jena.jar", "-q", tmpIn, "-o", tmpOut])

    # read the output
    tmpOutFile = open(tmpOut, "r")
    results = tmpOutFile.read()
    tmpOutFile.close()
    
    # remove the temporaryfile
    os.remove(tmpIn)
    os.remove(tmpOut)

    # return
    return results


# main
if __name__ == "__main__":

    # configure logger
    logging.basicConfig(level=logging.DEBUG)
    logging.info("SPARQLgenerate service starting...")

    # read the config file
    app.run(host='0.0.0.0', port=int(environ.get("PORT", 5000)))
